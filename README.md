# complex-event-detector

The problem of event detection arises in various application fields, ranging from data processing in Web environments, over robotics and transport, to finance and medicine. 
Real-time Complex Event Detection (CED) in turn, refers to the problem of identifying complex/composite events of interest, that are compositions of simple-events satisfying some pattern in a real-time fashion.
This is typically done to support reactive and proactive measures. Examples include the recognition of financial frauds, malfunctioning on cyberphysical systems, trend detection on social networks, outbreak of epidemics.
In this scenario, CED allows to extract knowledge from big event data streams and quickly react with appropriate actions. The goal of this thesis is to provide design methodologies for developing CED systems. 
This has been done by performing an extensive survey on state-of-the-art architectures and by designing and implementing a system that is capable of identifying complexevents from a collection of heterogeneous input data streams in nearly realtime, providing fault-tolerance and horizontal scalability. 
The developed system is experimentally evaluated to verify that the requirements for a CED application are satisfied and to point out eventual weaknesses.

(check the /docs directory for more infos)