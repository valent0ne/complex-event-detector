#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import logging
import math
import os
import random
import sys
from configparser import ConfigParser

class Generator:
    
    config = None
    out_directory = os.path.dirname(os.path.realpath(__file__)) + "/out/"
    out_filename = "data.csv"

    num_rows = 1
    lower_bound = 0
    upper_bound = 1
    # number of rows between values that falls outside the min-max bounds
    # e.g., if rows_between_outliers equals 0.2 and num_rows equals 1000,
    # then there will be an outlier every 200 rows
    rows_between_outliers = 1

    # 0=no noise, i.e., outliers will be placed exactly where it is indicated by the rows_between_outliers parameter
    # 1=maximum noise
    # example: rows_between_outliers=100, noise_ratio=0.1
    # -> outliers will be placed at rows corresponding to 90-110, 190-210...
    noise_ratio = 0

    def __init__(self):
        """
        initialize the application by loading properties from the configuration file
        """
        # read configuration file name from command line argument
        if len(sys.argv) < 2:
            logging.info("Missing argument: configuratiojn file name.")
            sys.exit(1)
        
        # instantiating the configuration parser
        self.config = ConfigParser()
        config_file_path = os.path.dirname(os.path.realpath(__file__)) +"/configs/"+ sys.argv[1]+".ini"
        self.config.read(config_file_path)

        invalid_values = [None, "null", "None", ""]

        # reading configuration properties from config.ini
        if self.config['general']['out_directory'] not in invalid_values:
            self.out_directory = self.config['general']['out_directory']

        if self.config['general']['out_filename'] not in invalid_values:
            self.out_filename = self.config['general']['out_filename']

        if self.config['generator']['num_rows'] not in invalid_values:
            self.num_rows = int(self.config['generator']['num_rows'])

        if self.config['generator']['lower_bound'] not in invalid_values:
            self.lower_bound = float(self.config['generator']['lower_bound'])

        if self.config['generator']['upper_bound'] not in invalid_values:
            self.upper_bound = float(self.config['generator']['upper_bound'])

        if self.config['generator']['upper_bound'] not in invalid_values:
            self.rows_between_outliers = float(self.config['generator']['rows_between_outliers'])

        if self.config['generator']['noise_ratio'] not in invalid_values:
            self.noise_ratio = float(self.config['generator']['noise_ratio'])

        logging.debug("configuration values: ")
        logging.debug("out_directory: {}".format(self.out_directory))
        logging.debug("out_filename: {}".format(self.out_filename))
        logging.debug("num_rows: {}".format(self.num_rows))
        logging.debug("lower_bound: {}".format(self.lower_bound))
        logging.debug("upper_bound: {}".format(self.upper_bound))
        logging.debug("rows_between_outliers: {}".format(self.rows_between_outliers))
        logging.debug("noise_ratio: {}".format(self.noise_ratio))

    def clean(self):
        """
        deletes previously generated files
        """
        if os.path.exists(self.out_directory + self.out_filename):
            os.remove(self.out_directory + self.out_filename)
        else:
            pass

    def generate(self):
        """
        generates the dataset
        """
        with open(self.out_directory + self.out_filename, 'w') as csv_file:
            number_of_outliers = 0
            file_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            file_writer.writerow(['identifier', 'value'])

            target_row = 0
            logging.debug("target_row: {}".format(target_row))
            for i in range(self.num_rows):
                identifier = i
                # check if I have reached a row in which I have to put an outlier
                if i == target_row:
                    number_of_outliers += 1
                    value = random.uniform(self.upper_bound, 2 * self.upper_bound)
                    # update target row based on noise value
                    target_row = i + math.floor(self.rows_between_outliers + self.get_noise_value())
                    logging.debug("target_row: {}".format(target_row))
                else:
                    value = random.uniform(self.lower_bound, self.upper_bound)

                file_writer.writerow([identifier, value])

            logging.info("number of outliers: {} ({}%)"
                         .format(str(number_of_outliers), str((number_of_outliers/self.num_rows)*100)))


    def get_noise_value(self):
        """
        :return: + or - noise (calculated as rows_between_outliers*noise_ratio)
        """
        sign = bool(int(random.randrange(0, 1, 1)))
        noise = random.uniform(0, self.rows_between_outliers * self.noise_ratio)
        return noise if sign else noise * -1


# entry point
if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logging.info("initializing...")
    g = Generator()

    logging.info("cleaning...")
    g.clean()

    logging.info("generating...")
    g.generate()
