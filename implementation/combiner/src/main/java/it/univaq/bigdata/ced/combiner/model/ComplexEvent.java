package it.univaq.bigdata.ced.combiner.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public class ComplexEvent extends Event {

    private List<SimpleEvent> triggers;
    private Instant fetchTimestamp;
    private Instant detectionTimestamp;

    public ComplexEvent() {
    }

    public ComplexEvent(String identifier, Instant fetchTimestamp, List<SimpleEvent> triggers) {
        super(getRandomIdentifier(identifier));
        this.triggers = triggers;
        this.fetchTimestamp = fetchTimestamp;
        this.detectionTimestamp = Instant.now();
    }

    @Override
    public void setIdentifier(String identifier){
        super.setIdentifier(getRandomIdentifier(identifier));
    }

    public List<SimpleEvent> getTriggers() {
        return triggers;
    }

    public void setTriggers(List<SimpleEvent> triggers) {
        this.triggers = triggers;
    }

    public Instant getFetchTimestamp() {
        return fetchTimestamp;
    }

    public void setFetchTimestamp(Instant fetchTimestamp) {
        this.fetchTimestamp = fetchTimestamp;
    }

    public Instant getDetectionTimestamp() {
        return detectionTimestamp;
    }

    public void setDetectionTimestamp(Instant detectionTimestamp) {
        this.detectionTimestamp = detectionTimestamp;
    }

    @Override
    public String toString() {
        return "ComplexEvent{" +
                "triggers=" + triggers +
                ", fetchTimestamp=" + fetchTimestamp +
                ", detectionTimestamp=" + detectionTimestamp +
                '}';
    }

    private static String getRandomIdentifier(String identifier){
        return "ce:"+identifier;
    }
}
