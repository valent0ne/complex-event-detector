package it.univaq.bigdata.ced.combiner.detector;

import it.univaq.bigdata.ced.combiner.config.CombinerConfiguration;
import it.univaq.bigdata.ced.combiner.model.ComplexEvent;
import it.univaq.bigdata.ced.combiner.model.SimpleEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.*;

public class OneValidSimpleEventPerTypeDetector implements Detector {

    private Map<String, List<SimpleEvent>> simpleEventsMap;
    private Map<String, SimpleEvent> candidates;
    private Long complexEventSize = CombinerConfiguration.COMPLEX_EVENT_SIZE;
    private Long counter = 0L;

    private static Logger LOGGER = LoggerFactory.getLogger(OneValidSimpleEventPerTypeDetector.class);

    public OneValidSimpleEventPerTypeDetector() {
        simpleEventsMap = new HashMap<>();
        for(String type : CombinerConfiguration.SIMPLE_EVENT_TYPES){
            simpleEventsMap.put(type, new ArrayList<>());
        }
        candidates = new HashMap<>();
    }

    public ComplexEvent process(SimpleEvent simpleEvent){

        ComplexEvent complexEvent = null;

        //LOGGER.info("Detecting complex event...");
        // update the respective simple events list by adding the current simple event
        simpleEventsMap.get(simpleEvent.getType()).add(simpleEvent);

        /*
        // this check has been moved into the next loop
        // remove all expired simple events from the hashmap
        for(Map.Entry<String, List<SimpleEvent>> entry: simpleEventsMap.entrySet()){
            entry.getValue().removeIf(se -> Instant.now().isAfter(se.getExpiryTimestamp()));
        }*/

        // check if there is at least 1 simple event per type that is currently active
        Instant now = Instant.now();
        for(Map.Entry<String, List<SimpleEvent>> entry: simpleEventsMap.entrySet()){
            // remove expired events
            entry.getValue().removeIf(se -> now.isAfter(se.getExpiryTimestamp()));


            //LOGGER.info("Simple events of type "+entry.getKey()+" currently valid: "+entry.getValue().size());
            // if in the current simple event list (of type x) there are more than 1 items, pick the first one
            if (entry.getValue().size() >= 1){
                //LOGGER.info("Added candidate of type "+entry.getKey());
                candidates.put(entry.getKey(), entry.getValue().get(0));
            }
        }
        // if there are n valid simple events (one per type) then there is a complex event
        if(candidates.keySet().size() == complexEventSize){
            // build the complex event
            complexEvent = new ComplexEvent((counter++).toString(), null, new ArrayList<>(candidates.values()));
            //LOGGER.info("*** Complex event {} detected ***", complexEvent.getIdentifier());
            
            // remove the candidates from the simple events map
            //LOGGER.info("Cleaning simple events...");
            for (Map.Entry<String, SimpleEvent> entry: candidates.entrySet()){
                simpleEventsMap.get(entry.getKey()).remove(entry.getValue());
            }
            // empty the candidates map
            //LOGGER.info("Cleaning candidates...");
            candidates.clear();

        }else { // else nothing happens
            //LOGGER.info("Complex event not triggered.");
        }
        return complexEvent;
    }
}
