package it.univaq.bigdata.ced.combiner.consumer;

import it.univaq.bigdata.ced.combiner.config.CombinerConfiguration;
import it.univaq.bigdata.ced.combiner.detector.Detector;
import it.univaq.bigdata.ced.combiner.detector.OneValidSimpleEventPerTypeDetector;
import it.univaq.bigdata.ced.combiner.model.ComplexEvent;
import it.univaq.bigdata.ced.combiner.model.SimpleEvent;
import it.univaq.bigdata.ced.combiner.repository.CouchbaseRepository;
import it.univaq.bigdata.ced.combiner.repository.Repository;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Properties;
import java.util.UUID;

public class SimpleEventConsumer {

    private static Logger LOGGER = LoggerFactory.getLogger(SimpleEventConsumer.class);
    private Repository repository;
    private Consumer<String, SimpleEvent> consumer;
    private Detector detector;
    private Duration pollTimeout;
    private Instant fetchTimestamp;

    public SimpleEventConsumer() {
        LOGGER.info("Instantiating consumer...");
        consumer = getConsumer();
        pollTimeout = Duration.ofMillis(CombinerConfiguration.KAFKA_POLL_TIMEOUT);

        LOGGER.info("Initializing detector...");
        detector = new OneValidSimpleEventPerTypeDetector();

        LOGGER.info("Opening connection to database...");
        repository = new CouchbaseRepository();
        repository.connect();
    }

    /**
     * Starts the consuming process
     */
    public void run() {

        LOGGER.info("Starting consumption now...");

        try {
            while (true) {
                consume();
            }
        }catch (Exception ex){
            LOGGER.error(ex.getMessage());
            ex.printStackTrace();
        }finally {
            consumer.close();
        }
    }


    /**
     * @return initialized kafka consumer
     */
    private Consumer<String, SimpleEvent> getConsumer() {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, CombinerConfiguration.KAFKA_ADDRESS);
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        properties.put(ConsumerConfig.CLIENT_ID_CONFIG, CombinerConfiguration.KAFKA_CLIENT_ID);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString());
        properties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, Integer.MAX_VALUE);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, SimpleEventDeserializer.class.getName());

        final KafkaConsumer<String, SimpleEvent> kafkaConsumer = new KafkaConsumer<>(properties);

        kafkaConsumer.subscribe(Collections.singletonList(CombinerConfiguration.KAFKA_TOPIC));

        return kafkaConsumer;
    }


    private void consume(){
        ConsumerRecords<String, SimpleEvent> records = consumer.poll(pollTimeout);

        for (ConsumerRecord<String, SimpleEvent> record : records){
            //LOGGER.info("fetched record: \noffset = {}, key = {}, value = {}", record.offset(), record.key(), record.value());

            fetchTimestamp = Instant.now();
            ComplexEvent complexEvent;
            if((complexEvent = detector.process(record.value())) != null){
                complexEvent.setFetchTimestamp(fetchTimestamp);
                repository.save(complexEvent);
            }

            //LOGGER.info((++processedEvents).toString());

            // save received simple event even if it does not generate a complex event
            //repository.save(record.value());
        }
    }
}
