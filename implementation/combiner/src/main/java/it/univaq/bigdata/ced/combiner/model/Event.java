package it.univaq.bigdata.ced.combiner.model;

public class Event {

    private String identifier;

    public Event(){}

    public Event(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
