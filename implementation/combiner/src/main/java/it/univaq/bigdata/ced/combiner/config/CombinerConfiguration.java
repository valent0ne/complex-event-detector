package it.univaq.bigdata.ced.combiner.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class CombinerConfiguration {
    private static Logger LOGGER = LoggerFactory
            .getLogger(CombinerConfiguration.class);

    private static Properties properties;

    public static String KAFKA_ADDRESS;
    public static String KAFKA_CLIENT_ID;
    public static String KAFKA_TOPIC;
    public static Long KAFKA_POLL_TIMEOUT;

    public static String[] SIMPLE_EVENT_TYPES;
    public static Long COMPLEX_EVENT_SIZE;

    public static String COUCHBASE_ADDRESS;
    public static String COUCHBASE_BUCKET_NAME;
    public static String COUCHBASE_BUCKET_PASSWORD;
    public static Long COUCHBASE_TIMEOUT;


    public static void init(String propertiesFilename) {
        try (InputStream input = CombinerConfiguration.class
                .getClassLoader().getResourceAsStream(propertiesFilename+".properties")) {

            properties = new Properties();

            if (input == null) {
                LOGGER.error("Unable to find "+propertiesFilename+".properties");
                return;
            }

            //load a properties file from class path, inside static method
            properties.load(input);

            KAFKA_ADDRESS = properties.getProperty("kafka.address");
            KAFKA_CLIENT_ID = properties.getProperty("kafka.client-id");
            KAFKA_TOPIC = properties.getProperty("kafka.topic");
            KAFKA_POLL_TIMEOUT = Long.parseLong(properties.getProperty("kafka.poll-timeout"));

            SIMPLE_EVENT_TYPES = properties.getProperty("simple-event.types").split(",");
            COMPLEX_EVENT_SIZE = Long.parseLong(properties.getProperty("complex-event.size"));

            COUCHBASE_ADDRESS = properties.getProperty("couchbase.address");
            COUCHBASE_BUCKET_NAME = properties.getProperty("couchbase.bucket.name");
            COUCHBASE_BUCKET_PASSWORD = properties.getProperty("couchbase.bucket.password");
            COUCHBASE_TIMEOUT = Long.parseLong(properties.getProperty("couchbase.timeout"));

        } catch (IOException ex) {
            LOGGER.error("Error while loading properties.");
            ex.printStackTrace();
        }
    }

    public static String get(String propertyName){
        return properties.getProperty(propertyName);
    }
}
