package it.univaq.bigdata.ced.combiner.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.univaq.bigdata.ced.combiner.model.SimpleEvent;
import org.apache.kafka.common.serialization.Deserializer;

import java.time.Instant;

public class SimpleEventDeserializer implements Deserializer {

    ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();

    @Override public void close() {
    }

    @Override
    public SimpleEvent deserialize(String arg0, byte[] arg1) {

        SimpleEvent simpleEvent = null;
        try {
            simpleEvent = mapper.readValue(arg1, SimpleEvent.class);
            simpleEvent.setCombinerTimestamp(Instant.now());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return simpleEvent;
    }


}
