package it.univaq.bigdata.ced.combiner.repository;

import it.univaq.bigdata.ced.combiner.model.Event;

public interface Repository {

    void connect();
    void save(Event e);
    void disconnect();

}
