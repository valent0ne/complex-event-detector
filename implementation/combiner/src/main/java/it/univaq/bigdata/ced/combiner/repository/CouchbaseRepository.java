package it.univaq.bigdata.ced.combiner.repository;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import it.univaq.bigdata.ced.combiner.config.CombinerConfiguration;
import it.univaq.bigdata.ced.combiner.model.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;


public class CouchbaseRepository implements Repository {

    private static ObjectMapper objectMapper;
    private Cluster cluster;
    private Bucket bucket;

    private static Logger LOGGER = LoggerFactory.getLogger(CouchbaseRepository.class);


    public CouchbaseRepository(){
        objectMapper = new ObjectMapper().findAndRegisterModules();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    public void connect(){
        cluster = CouchbaseCluster.create(CombinerConfiguration.COUCHBASE_ADDRESS);
        bucket = cluster.openBucket(CombinerConfiguration.COUCHBASE_BUCKET_NAME,
                CombinerConfiguration.COUCHBASE_BUCKET_PASSWORD);
    }

    public void save(Event e){
        JsonDocument doc = null;
        try {
            doc = JsonDocument
                    .create(e.getIdentifier(), JsonObject.fromJson(objectMapper.writeValueAsString(e)));
            bucket.insert(doc);
        } catch (JsonProcessingException ex) {
            LOGGER.info("Error while serializing the object to json.");
            ex.printStackTrace();
        }
    }

    public void disconnect(){
        cluster.disconnect();
    }
}
