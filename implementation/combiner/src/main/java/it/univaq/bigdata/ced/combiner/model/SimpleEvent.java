package it.univaq.bigdata.ced.combiner.model;


import java.time.Instant;
import java.util.UUID;

public class SimpleEvent extends Event {

    private Double value;
    private String type;
    private Instant emitterTimestamp;
    private Instant analyserTimestampStart;
    private Instant analyserTimestampEnd;
    private Instant expiryTimestamp;
    private Instant combinerTimestamp;

    public SimpleEvent() {
    }

    public SimpleEvent(String identifier,
                       Double value,
                       String type,
                       Instant emitterTimestamp,
                       Instant analyserTimestampStart,
                       Instant analyserTimestampEnd,
                       Instant expiryTimestamp,
                       Instant combinerTimestamp) {

        super(getRandomIdentifier(identifier));
        this.value = value;
        this.type = type;
        this.emitterTimestamp = emitterTimestamp;
        this.analyserTimestampStart = analyserTimestampStart;
        this.analyserTimestampEnd = analyserTimestampEnd;
        this.expiryTimestamp = expiryTimestamp;
        this.combinerTimestamp = combinerTimestamp;
    }

    @Override
    public void setIdentifier(String identifier){
        super.setIdentifier(getRandomIdentifier(identifier));
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Instant getEmitterTimestamp() {
        return emitterTimestamp;
    }

    public void setEmitterTimestamp(Instant emitterTimestamp) {
        this.emitterTimestamp = emitterTimestamp;
    }

    public Instant getAnalyserTimestampStart() {
        return analyserTimestampStart;
    }

    public void setAnalyserTimestampStart(Instant analyserTimestampStart) {
        this.analyserTimestampStart = analyserTimestampStart;
    }

    public Instant getAnalyserTimestampEnd() {
        return analyserTimestampEnd;
    }

    public void setAnalyserTimestampEnd(Instant analyserTimestampEnd) {
        this.analyserTimestampEnd = analyserTimestampEnd;
    }

    public Instant getExpiryTimestamp() {
        return expiryTimestamp;
    }

    public void setExpiryTimestamp(Instant expiryTimestamp) {
        this.expiryTimestamp = expiryTimestamp;
    }

    public Instant getCombinerTimestamp() {
        return combinerTimestamp;
    }

    public void setCombinerTimestamp(Instant combinerTimestamp) {
        this.combinerTimestamp = combinerTimestamp;
    }

    private static String getRandomIdentifier(String identifier){
        return "se:"+identifier;
    }

    @Override
    public String toString() {
        return "SimpleEvent{" +
                "value=" + value +
                ", type='" + type + '\'' +
                ", emitterTimestamp=" + emitterTimestamp +
                ", analyserTimestampStart=" + analyserTimestampStart +
                ", analyserTimestampEnd=" + analyserTimestampEnd +
                ", expiryTimestamp=" + expiryTimestamp +
                ", combinerTimestamp=" + combinerTimestamp +
                '}';
    }
}
