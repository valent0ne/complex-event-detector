package it.univaq.bigdata.ced.combiner;

import it.univaq.bigdata.ced.combiner.consumer.SimpleEventConsumer;
import it.univaq.bigdata.ced.combiner.config.CombinerConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application's entry point
 */
public class App {

    private static Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {

        if(args.length < 1){
            LOGGER.error("Missing argument: properties file name.");
        }

        LOGGER.info("Loading configuration...");
        CombinerConfiguration.init(args[0]);

        LOGGER.info("Running consumer...");
        SimpleEventConsumer consumer = new SimpleEventConsumer();
        consumer.run();
    }
}
