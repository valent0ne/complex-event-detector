package it.univaq.bigdata.ced.combiner.detector;

import it.univaq.bigdata.ced.combiner.model.ComplexEvent;
import it.univaq.bigdata.ced.combiner.model.SimpleEvent;

public interface Detector {
    ComplexEvent process(SimpleEvent simpleEvent);
}
