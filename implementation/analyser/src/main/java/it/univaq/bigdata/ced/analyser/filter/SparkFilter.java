package it.univaq.bigdata.ced.analyser.filter;

import it.univaq.bigdata.ced.analyser.config.AnalyserConfiguration;

import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.streaming.Trigger;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.spark.sql.functions.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;

public class SparkFilter {

    private SparkSession sparkSession;
    private StructType schema;
    private String outputMode;

    private static Logger LOGGER = LoggerFactory.getLogger(SparkFilter.class);

    private static final String COLUMN_ANALYSER_TIMESTAMP_UDF_NAME = "getInstant";
    private static final String COLUMN_ANALYSER_TIMESTAMP_END_NAME = "analyserTimestampEnd";
    private static final String COLUMN_ANALYSER_TIMESTAMP_START_NAME = "analyserTimestampStart";
    private static final String COLUMN_EXPIRY_TIMESTAMP_UDF_NAME = "getExpiryInstant";
    private static final String COLUMN_EXPIRY_TIMESTAMP_NAME = "expiryTimestamp";
    private static final String COLUMN_TYPE_NAME = "type";


    public SparkFilter(String outputMode) {

        this.outputMode = outputMode;

        // initialise spark session
        LOGGER.info("Initializing spark session...");
        sparkSession = SparkSession
                .builder()
                .appName(AnalyserConfiguration.SPARK_APP_NAME)
                .master(AnalyserConfiguration.SPARK_MASTER)
                .getOrCreate();

        sparkSession.sparkContext().setLogLevel("ERROR");

        // register user defined function to compute the current timestamp
        LOGGER.info("Registering user defined function(s)...");

        // this function returns the current Instant
        sparkSession.udf().register(
                COLUMN_ANALYSER_TIMESTAMP_UDF_NAME,
                (UDF1<Long, String>) (identifier) -> Instant.now().toString(), DataTypes.StringType
        );

        //this function returns the expiry Instant given the analyserTimestamp
        long validity = AnalyserConfiguration.DATA_VALIDITY;
        sparkSession.udf().register(
                COLUMN_EXPIRY_TIMESTAMP_UDF_NAME,
                (UDF1<String, String>) (analyserTimestamp) ->
                        Instant
                                .parse(analyserTimestamp)
                                .plus(validity, ChronoUnit.MILLIS)
                                .toString(), DataTypes.StringType
        );

        LOGGER.info("Retrieving schema...");
        // create a dummy dataset from a sample record .json file so to infer its structure
        schema = sparkSession
                .read()
                .json(sparkSession
                        .createDataset(Collections
                                .singletonList(AnalyserConfiguration.jsonSchema), Encoders.STRING()))
                .schema();

        schema.printTreeString();

        // prepare schema (manually)
        /*
        schema = new StructType();
        schema.add("identifier", DataTypes.LongType);
        schema.add("value", DataTypes.DoubleType);
        schema.add("emitterTimestamp", DataTypes.TimestampType);
        */

    }

    public void run() throws StreamingQueryException {

        Dataset<Row> df = sparkSession
                // read from kafka topic
                .readStream()
                .format("kafka")
                .option("kafka.bootstrap.servers", AnalyserConfiguration.KAFKA_ADDRESS_SOURCE)
                .option("subscribe", AnalyserConfiguration.KAFKA_TOPIC_SOURCE)
                .option("startingOffsets", AnalyserConfiguration.KAFKA_STARTING_OFFSET)
                //.option("failOnDataLoss", false)
                .load()
                // start filtering process
                // read the field "value" from the kafka topic as string
                .selectExpr("CAST(value AS STRING)")
                // apply the schema to the "value" field (it contains the whole object)
                .select(from_json(col("value"), schema).as("record"))
                // the resulting json from the previous step will be inside the "record" field
                // so we need to flatten the new structure by navigating into the "record" field
                .select("record.*")
                // apply the filter
                // add the analyser start timestamp column by calling the respective UDF
                .withColumn(COLUMN_ANALYSER_TIMESTAMP_START_NAME,
                        functions.callUDF(COLUMN_ANALYSER_TIMESTAMP_UDF_NAME,
                                col(AnalyserConfiguration.DATA_COLUMN_IDENTIFIER_NAME)))
                // run filter
                .filter(col(AnalyserConfiguration.DATA_COLUMN_VALUE_NAME)
                        .$less(AnalyserConfiguration.FILTER_LOWER_BOUND)
                        .or(col(AnalyserConfiguration.DATA_COLUMN_VALUE_NAME)
                                .$greater(AnalyserConfiguration.FILTER_UPPER_BOUND)))
                // add the type column by calling the respective UDF
                .withColumn(COLUMN_TYPE_NAME,
                        lit(AnalyserConfiguration.DATA_TYPE).cast(DataTypes.StringType))
                // add the expiry timestamp column by calling the respective UDF
                .withColumn(COLUMN_EXPIRY_TIMESTAMP_NAME,
                        functions.callUDF(COLUMN_EXPIRY_TIMESTAMP_UDF_NAME,
                                col(COLUMN_ANALYSER_TIMESTAMP_START_NAME)))
                // add the analyser end timestamp column by calling the respective UDF
                .withColumn(COLUMN_ANALYSER_TIMESTAMP_END_NAME,
                        functions.callUDF(COLUMN_ANALYSER_TIMESTAMP_UDF_NAME,
                                col(AnalyserConfiguration.DATA_COLUMN_IDENTIFIER_NAME)));

        // explain the plan (print it)
        //df.explain(true);

        // output the result to the console
        if (outputMode.equals("console")) {
            StreamingQuery output = df
                    .writeStream()
                    .outputMode("append")
                    .format("console")
                    .option("truncate", "false")
                    .trigger(Trigger.Continuous(AnalyserConfiguration.SPARK_CHECKPOINT_INTERVAL))
                    .start();
            output.awaitTermination();

            // output the result to the kafka topic
        } else if (outputMode.equals("kafka")) {
            StreamingQuery output = df
                    // refactor the record for the submission to the kafka topic
                    .selectExpr("CAST(" + AnalyserConfiguration.DATA_COLUMN_IDENTIFIER_NAME + " AS STRING) AS key",
                            "to_json(struct(*)) AS value")
                    .writeStream()
                    .format("kafka")
                    .option("kafka.bootstrap.servers", AnalyserConfiguration.KAFKA_ADDRESS_SINK)
                    .option("topic", AnalyserConfiguration.KAFKA_TOPIC_SINK)
                    .option("checkpointLocation", AnalyserConfiguration.SPARK_CHECKPOINT_DIRECTORY)
                    .trigger(Trigger.Continuous(AnalyserConfiguration.SPARK_CHECKPOINT_INTERVAL))
                    .start();
            output.awaitTermination();

        } else {
            throw new IllegalArgumentException("outputmode parameter not valid");
        }


        // ******EXTENDED VERSION OF THE IMPLEMENTATION ABOVE******

        /*
        Dataset<Row> inputDf = sparkSession
                // Read from kafka topic
                .readStream()
                .format("kafka")
                .option("kafka.bootstrap.servers", AnalyserConfiguration.KAFKA_ADDRESS)
                .option("subscribe", AnalyserConfiguration.KAFKA_SOURCE_TOPIC)
                .option("startingOffsets", AnalyserConfiguration.KAFKA_STARTING_OFFSET)
                .option("failOnDataLoss", false)
                .option("kafkaConsumer.pollTimeoutMs", Long.MAX_VALUE)
                .option("fetchOffset.numRetries", Integer.MAX_VALUE)
                .option("fetchOffset.retryIntervalMs", Long.MAX_VALUE)
                .load();
        inputDf.printSchema();

        // interpret the value in the kafka topic as a string
        Dataset<Row> inputDfAsString = inputDf.selectExpr("CAST(value AS STRING)");

        // convert it to json using the schema inferred in the init() function
        Dataset<Row> inputDfAsJson = inputDfAsString
                .select(from_json(inputDfAsString.col("value"), schema).as("record"));
        //inputDfAsJson.printSchema();

        // flatten the dataset by removing the previously created parent node
        Dataset<Row> inputDfAsJsonFlattened = inputDfAsJson.select("record.*");
        //inputDfAsJsonFlattened.printSchema();

        // keep only outliers (rows with value greater than the upperbound or lower than the lowerbound)
        Dataset<Row> filteredDf = inputDfAsJsonFlattened
                .filter(inputDfAsJsonFlattened.col(AnalyserConfiguration.DATA_COLUMN_VALUE_NAME)
                        .$less(AnalyserConfiguration.FILTER_LOWER_BOUND)
                        .or(inputDfAsJsonFlattened.col(AnalyserConfiguration.DATA_COLUMN_VALUE_NAME)
                                .$greater(AnalyserConfiguration.FILTER_UPPER_BOUND)));

        // output dataset with timestamps, the timestamp is computed using the user defined function
        // created in the constructor
        Dataset<Row> dfWithAnalyserTimestamp = filteredDf
                .withColumn(COLUMN_ANALYSER_TIMESTAMP_NAME,
                        functions.callUDF(COLUMN_ANALYSER_TIMESTAMP_UDF_NAME,
                                col(AnalyserConfiguration.DATA_COLUMN_IDENTIFIER_NAME)));

        // add expiry timestamp
        Dataset<Row> dfWithExpiryTimestamp = dfWithAnalyserTimestamp
                .withColumn(COLUMN_EXPIRY_TIMESTAMP_NAME,
                        functions.callUDF(COLUMN_EXPIRY_TIMESTAMP_UDF_NAME,
                                col(COLUMN_ANALYSER_TIMESTAMP_NAME)));

        // add type column
        Dataset<Row> finalDf = dfWithExpiryTimestamp
                .withColumn(COLUMN_TYPE_NAME,
                        lit(AnalyserConfiguration.DATA_TYPE).cast(DataTypes.StringType));

        // output the result to the console
        if (outputMode.equals("console")) {
            StreamingQuery output = finalDf
                    .writeStream()
                    .outputMode("append")
                    .format("console")
                    .option("truncate", "false")
                    .trigger(Trigger.Continuous(AnalyserConfiguration.SPARK_CHECKPOINT_INTERVAL))
                    .start();
            output.awaitTermination();

            // output the result to the kafka topic
        } else if (outputMode.equals("kafka")) {
            StreamingQuery output = finalDf
                    .selectExpr("CAST(" + AnalyserConfiguration.DATA_COLUMN_IDENTIFIER_NAME + " AS STRING) AS key",
                            "to_json(struct(*)) AS value")
                    .writeStream()
                    .format("kafka")
                    .option("kafka.bootstrap.servers", AnalyserConfiguration.KAFKA_ADDRESS)
                    .option("topic", AnalyserConfiguration.KAFKA_SINK_TOPIC)
                    .option("checkpointLocation", AnalyserConfiguration.SPARK_CHECKPOINT_DIRECTORY)
                    .trigger(Trigger.Continuous(AnalyserConfiguration.SPARK_CHECKPOINT_INTERVAL))
                    .start();
            output.awaitTermination();

        } else {
            throw new IllegalArgumentException("outputmode parameter not valid");
        }*/
    }
}
