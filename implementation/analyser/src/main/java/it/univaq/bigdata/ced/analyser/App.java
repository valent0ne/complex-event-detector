package it.univaq.bigdata.ced.analyser;

import it.univaq.bigdata.ced.analyser.config.AnalyserConfiguration;
import it.univaq.bigdata.ced.analyser.filter.SparkFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.log4j.PropertyConfigurator;

/**
 * Application's entry point
 */
public class App {

    private static Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {

        PropertyConfigurator.configure(App.class.getClassLoader().getResourceAsStream("log4j.properties"));

        if(args.length < 2){
            LOGGER.error("Missing argument: properties file name/output mode");
        }

        LOGGER.info("Loading configuration...");
        AnalyserConfiguration.init(args[0]);

        LOGGER.info("Running consumer...");
        SparkFilter consumer = new SparkFilter(args[1]);

        try{
            consumer.run();
        }catch (Exception e){
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
