package it.univaq.bigdata.ced.analyser.config;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;

public class AnalyserConfiguration {
    private static Logger LOGGER = LoggerFactory
            .getLogger(AnalyserConfiguration.class);

    private static Properties properties;
    public static String jsonSchema;

    public static String KAFKA_ADDRESS_SOURCE;
    public static String KAFKA_ADDRESS_SINK;

    public static String KAFKA_TOPIC_SOURCE;
    public static String KAFKA_TOPIC_SINK;
    public static String KAFKA_STARTING_OFFSET;

    public static String SPARK_HADOOP_HOME_DIR;
    public static String SPARK_APP_NAME;
    public static String SPARK_MASTER;
    public static Long SPARK_CHECKPOINT_INTERVAL;
    public static String SPARK_CHECKPOINT_DIRECTORY;

    public static Double FILTER_UPPER_BOUND;
    public static Double FILTER_LOWER_BOUND;

    public static String DATA_TYPE;
    public static Long DATA_VALIDITY;
    public static String DATA_COLUMN_IDENTIFIER_NAME;
    public static String DATA_COLUMN_VALUE_NAME;


    /**
     * load the content of the .properties file into a Properties object
     */
    public static void init(String propertiesFilename) {

        try {
            jsonSchema = IOUtils.toString(Objects.requireNonNull(AnalyserConfiguration
                    .class
                    .getClassLoader()
                    .getResourceAsStream("record.json")), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }



        try (InputStream input = AnalyserConfiguration.class
                .getClassLoader().getResourceAsStream(propertiesFilename+".properties")) {

            properties = new Properties();

            if (input == null) {
                LOGGER.error("Unable to find "+propertiesFilename+".properties");
                throw new FileNotFoundException();
            }

            //load a properties file from class path
            properties.load(input);

            KAFKA_ADDRESS_SOURCE = properties.getProperty("kafka.address.source");
            KAFKA_ADDRESS_SINK = properties.getProperty("kafka.address.sink");

            KAFKA_TOPIC_SOURCE = properties.getProperty("kafka.topic.source");
            KAFKA_TOPIC_SINK = properties.getProperty("kafka.topic.sink");
            KAFKA_STARTING_OFFSET = properties.getProperty("kafka.starting-offset");

            SPARK_HADOOP_HOME_DIR = properties.getProperty("spark.hadoop-home-dir");
            SPARK_APP_NAME = properties.getProperty("spark.app-name");
            SPARK_MASTER = properties.getProperty("spark.master");
            SPARK_CHECKPOINT_INTERVAL = Long.parseLong(properties.getProperty("spark.checkpoint.interval"));
            SPARK_CHECKPOINT_DIRECTORY = properties.getProperty("spark.checkpoint.directory");

            FILTER_UPPER_BOUND = Double.parseDouble(properties.getProperty("filter.upper-bound"));
            FILTER_LOWER_BOUND = Double.parseDouble(properties.getProperty("filter.lower-bound"));

            DATA_TYPE = properties.getProperty("data.type");
            DATA_VALIDITY = Long.parseLong(properties.getProperty("data.validity"));
            DATA_COLUMN_IDENTIFIER_NAME = properties.getProperty("data.column.identifier.name");
            DATA_COLUMN_VALUE_NAME = properties.getProperty("data.column.value.name");

            System.setProperty("hadoop.home.dir", SPARK_HADOOP_HOME_DIR);

        } catch (IOException ex) {
            LOGGER.error("Error while loading properties.");
            ex.printStackTrace();
        }
    }

    /**
     * @param propertyName the property name
     * @return the value corresponding to the provided property name
     */
    public static String get(String propertyName){
        return properties.getProperty(propertyName);
    }
}
