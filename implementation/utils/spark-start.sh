#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Missing argument: properties file name."
    exit 1
fi

source ./conf/$1.properties

# run the spark master server
echo "starting master..."
$SPARK_HOME/sbin/start-master.sh -i 127.0.0.1 -p 7077 --webui-port 7078

echo "starting slave(s)..."
$SPARK_HOME/sbin/start-slave.sh spark://127.0.0.1:7077
#SPARK_WORKER_INSTANCES=3 SPARK_WORKER_CORES=1 $SPARK_HOME/sbin/start-slaves.sh spark://127.0.0.1:7077
