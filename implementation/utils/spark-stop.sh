#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Missing argument: properties file name."
    exit 1
fi

source ./conf/$1.properties

# run the spark slaves server
$SPARK_HOME/sbin/stop-slave.sh

# run the spark master server
$SPARK_HOME/sbin/stop-master.sh