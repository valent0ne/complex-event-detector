#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Missing argument: properties file name."
    exit 1
fi

source ./conf/$1.properties

rm -rf $ZOOKEEPER_FILES
rm -rf $KAFKA_FILES

