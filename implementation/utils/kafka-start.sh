#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Missing argument: properties file name."
    exit 1
fi

source ./conf/$1.properties

# run the kafka server
$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties
