#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Missing argument: properties file name."
    exit 1
fi

source ./conf/$1.properties

# run zookeeper
$ZOOKEEPER_HOME/bin/zookeeper-server-start.sh $ZOOKEEPER_HOME/config/zookeeper.properties

