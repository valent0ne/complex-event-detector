#!/bin/bash

if [ $# -lt 1 ]
  then
    echo "Missing argument: properties file name."
    exit 1
fi

source ./conf/$1.properties

# run the emitters in parallel
(echo java -jar $EMITTER_JAR_PATH emitter-$1-1; echo java -jar $EMITTER_JAR_PATH emitter-$1-2 ; echo java -jar $EMITTER_JAR_PATH emitter-$1-3;) | parallel
