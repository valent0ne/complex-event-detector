#!/bin/bash

if [ $# -lt 3 ]
  then
    echo "Missing arguments, provide: script properties file name, Spark application properties file name and output mode."
    exit 1
fi

source ./conf/$1.properties

echo "submitting spark application..."
# run the spark master server
$SPARK_HOME/bin/spark-submit \
            --class it.univaq.bigdata.ced.analyser.App \
            --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.4.4 \
            --master spark://127.0.0.1:7077 \
            --deploy-mode client \
            --conf spark.cores.max=1 \
            $ANALYSER_JAR_PATH $2 $3