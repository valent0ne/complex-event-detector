package it.univaq.bigdata.ced.emitter.model;

import java.time.Instant;

public class Record {

    private Long identifier;
    private Double value;
    private Instant emitterTimestamp;


    public Record() {
    }

    public Record(Long identifier, Double value) {
        this.identifier = identifier;
        this.value = value;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Instant getEmitterTimestamp() {
        return emitterTimestamp;
    }

    public void setEmitterTimestamp(Instant emitterTimestamp) {
        this.emitterTimestamp = emitterTimestamp;
    }

    public String toJsonString(){
        return "{\"identifier\":"+identifier.toString()
                +",\"value\":"+value.toString()
                +",\"emitterTimestamp\":\""+emitterTimestamp.toString()
                +"\"}";
    }

}
