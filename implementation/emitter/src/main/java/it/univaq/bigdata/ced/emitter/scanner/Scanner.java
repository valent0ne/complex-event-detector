package it.univaq.bigdata.ced.emitter.scanner;

import it.univaq.bigdata.ced.emitter.model.Record;

public interface Scanner {

    Record nextRecord();
}
