package it.univaq.bigdata.ced.emitter.scanner;


import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import it.univaq.bigdata.ced.emitter.config.EmitterConfiguration;
import it.univaq.bigdata.ced.emitter.model.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CSVScanner implements Scanner {

    private CsvParser csvReader;
    private CsvParserSettings settings;

    private static Logger LOGGER = LoggerFactory.getLogger(CSVScanner.class);


    /**
     * Initializes the Scanner
     */
    public CSVScanner() {
        settings = new CsvParserSettings();
        settings.detectFormatAutomatically();
        settings.setNumberOfRowsToSkip(1);

        try{
            LOGGER.info("Loading csv data file...");
            Reader reader = Files.newBufferedReader(Paths.get(EmitterConfiguration.DATA_PATH));
            // skip header row
            csvReader = new CsvParser(settings);
            csvReader.beginParsing(reader);
        }catch (Exception e){
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
    }


    /**
     *
     * @return the Record built from the csv row or null if there are errors
     */
    public Record nextRecord(){
        String[] nextRecord;
        Record r = null;
        try{
            if ((nextRecord = csvReader.parseNext()) != null) {
                r = new Record();
                r.setIdentifier(Long.parseLong(nextRecord[0]));
                r.setValue(Double.parseDouble(nextRecord[1]));
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
        return r;
    }



}
