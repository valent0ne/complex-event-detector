package it.univaq.bigdata.ced.emitter;

import it.univaq.bigdata.ced.emitter.producer.RecordProducer;
import it.univaq.bigdata.ced.emitter.config.EmitterConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application's entry point
 */
public class App {

    private static Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws InterruptedException {

        if(args.length < 1){
            LOGGER.error("Missing argument: properties file name.");
        }

        LOGGER.info("Loading configuration...");
        EmitterConfiguration.init(args[0]);

        LOGGER.info("Running producer...");
        RecordProducer producer = new RecordProducer();
        Thread.sleep(EmitterConfiguration.GENERAL_STARTUP_WAIT);
        producer.run();
    }
}
