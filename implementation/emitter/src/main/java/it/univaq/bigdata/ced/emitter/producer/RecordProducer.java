package it.univaq.bigdata.ced.emitter.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import it.univaq.bigdata.ced.emitter.config.EmitterConfiguration;
import it.univaq.bigdata.ced.emitter.model.Record;
import it.univaq.bigdata.ced.emitter.scanner.CSVScanner;
import it.univaq.bigdata.ced.emitter.scanner.Scanner;
import it.univaq.bigdata.ced.emitter.scanner.SimulatedScanner;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class RecordProducer {

    private static ObjectMapper objectMapper;
    private Producer<Long, String> producer;
    private Scanner scanner;
    private Runnable emittingTask;
    private static ScheduledExecutorService service;

    private static Logger LOGGER = LoggerFactory.getLogger(RecordProducer.class);

    public RecordProducer() {
        LOGGER.info("Instantiating producer...");
        producer = getProducer();

        LOGGER.info("Instantiating auxiliary classes...");

        objectMapper = new ObjectMapper().findAndRegisterModules();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        if(EmitterConfiguration.GENERAL_USE_SIMULATED_SCANNER){
            scanner = new SimulatedScanner();
        }else{
            scanner = new CSVScanner();
        }

        emittingTask = () -> {
            Record r;
            if ((r = scanner.nextRecord()) != null) {
                publish(r);
            } else {
                LOGGER.info("Input consumed.");
                service.shutdown();
            }
        };
    }

    /**
     * Starts the emitting process
     */
    public void run() {
        long wait = EmitterConfiguration.KAFKA_WAIT;
        LOGGER.info("Emitting...");

        if (wait > 0) {
            service = Executors.newSingleThreadScheduledExecutor();
            service.scheduleAtFixedRate(emittingTask, 0, wait, TimeUnit.MICROSECONDS);
        } else {
            Instant before = Instant.now();
            Record r;
            while ((r = scanner.nextRecord()) != null) {
                publish(r);
            }
            Instant after = Instant.now();
            LOGGER.info("Time to emit: {} ms", after.toEpochMilli() - before.toEpochMilli());
        }

    }


    /**
     * @return initialized kafka producer
     */
    private Producer<Long, String> getProducer() {
        Properties properties = new Properties();

        // set the broker ip:port address
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, EmitterConfiguration.KAFKA_ADDRESS);
        /*
        acks=0   If set to zero then the producer will not wait for any acknowledgment from the server at all.
                 The record will be immediately added to the socket buffer and considered sent. No guarantee can be made
                 that the server has received the record in this case, and the retries configuration will not take
                 effect (as the client won't generally know of any failures). The offset given back for each record will
                 always be set to -1.
        acks=1   This will mean the leader will write the record to its local log but will respond without awaiting full
                 acknowledgement from all followers. In this case should the leader fail immediately after acknowledging
                 the record but before the followers have replicated it then the record will be lost.
        acks=all This means the leader will wait for the full set of in-sync replicas to acknowledge the record.
                 This guarantees that the record will not be lost as long as at least one in-sync replica remains alive.
                 This is the strongest available guarantee. This is equivalent to the acks=-1 setting.
         */
        properties.put(ProducerConfig.ACKS_CONFIG, "0");

        /*
        The producer groups together any records that arrive in between request transmissions into a single batched
        request. Normally this occurs only under load when records arrive faster than they can be sent out. However in
        some circumstances the client may want to reduce the number of requests even under moderate load. This setting
        accomplishes this by adding a small amount of artificial delay—that is, rather than immediately sending out a
        record the producer will wait for up to the given delay to allow other records to be sent so that the sends can
        be batched together. This can be thought of as analogous to Nagle's algorithm in TCP. This setting gives the
        upper bound on the delay for batching: once we get batch.size worth of records for a partition it will be sent
        immediately regardless of this setting, however if we have fewer than this many bytes accumulated for this
        partition we will 'linger' for the specified time waiting for more records to show up. This setting defaults
        to 0 (i.e. no delay). Setting linger.ms=5, for example, would have the effect of reducing the number of requests
        sent but would add up to 5ms of latency to records sent in the absence of load.
         */
        properties.put(ProducerConfig.LINGER_MS_CONFIG, EmitterConfiguration.KAFKA_LINGER_MS);

        properties.put(ProducerConfig.BATCH_SIZE_CONFIG, EmitterConfiguration.KAFKA_BATCH_SIZE);

        // not necessary with acks=0
        //properties.put(ProducerConfig.RETRIES_CONFIG, 0);

        // an id string to pass to the server when making requests
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, EmitterConfiguration.KAFKA_CLIENT_ID);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        return new KafkaProducer<>(properties);
    }

    /**
     * @param r the Record to publish
     */
    private void publish(Record r) {

        r.setEmitterTimestamp(Instant.now());
        try {
            producer.send(new ProducerRecord<>(EmitterConfiguration.KAFKA_TOPIC,
                    r.getIdentifier(),
                    r.toJsonString()
                    /*objectMapper.writeValueAsString(r)*/));
        } catch (/*JsonProcessingException*/ Exception e) {
            LOGGER.info("Publish exception for record: {}", r);
            e.printStackTrace();
        }

        /* EXTENDED VERSION

        long key = r.getIdentifier();

        r.setEmitterTimestamp(Instant.now());
        String payload = null;
        try {
            payload = objectMapper.writeValueAsString(r);
        } catch (JsonProcessingException e) {
            LOGGER.info("Cannot serialize object: {}", r);
            e.printStackTrace();
        }
        ProducerRecord<Long, String> record =
                new ProducerRecord<>(EmitterConfiguration.KAFKA_TOPIC, key, payload);

        producer.send(record);
        */


    }
}
