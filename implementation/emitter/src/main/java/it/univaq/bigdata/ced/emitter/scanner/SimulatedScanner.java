package it.univaq.bigdata.ced.emitter.scanner;


import it.univaq.bigdata.ced.emitter.config.EmitterConfiguration;
import it.univaq.bigdata.ced.emitter.model.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class SimulatedScanner implements Scanner {

    private Long rowCounter;
    private Long nextOutlierRow;
    private Long numberOfRows;
    private Long rowsBetweenOutliers;
    private Double lowerBound;
    private Double upperBound;
    private Double value;
    private Random random;

    private static Logger LOGGER = LoggerFactory.getLogger(SimulatedScanner.class);


    /**
     * Initializes the Scanner
     */
    public SimulatedScanner() {
        random = new Random();
        rowCounter = 0L;
        nextOutlierRow = 0L;
        rowsBetweenOutliers = EmitterConfiguration.DATA_ROWS_BETWEEN_OUTLIERS;
        numberOfRows = EmitterConfiguration.DATA_NUM_ROWS;
        lowerBound = EmitterConfiguration.DATA_LOWER_BOUND;
        upperBound = EmitterConfiguration.DATA_UPPER_BOUND;
    }


    /**
     *
     * @return the Record built from the csv row or null if there are errors
     */
    public Record nextRecord(){
        if(numberOfRows.equals(rowCounter)){
            return null;
        }
        value = lowerBound + (upperBound - lowerBound) * random.nextDouble();
        if(rowCounter.equals(nextOutlierRow)){
            value += upperBound;
            nextOutlierRow+=rowsBetweenOutliers;
        }
        rowCounter++;
        return new Record(rowCounter, value);
    }

}
