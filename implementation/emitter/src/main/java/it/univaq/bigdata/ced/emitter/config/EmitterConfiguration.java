package it.univaq.bigdata.ced.emitter.config;

import org.apache.kafka.common.protocol.types.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class EmitterConfiguration {
    private static Logger LOGGER = LoggerFactory
            .getLogger(EmitterConfiguration.class);

    private static Properties properties;

    public static String KAFKA_ADDRESS;
    public static String KAFKA_CLIENT_ID;
    public static String KAFKA_TOPIC;
    public static Long KAFKA_WAIT;
    public static Long KAFKA_LINGER_MS;
    public static Integer KAFKA_BATCH_SIZE;

    public static String DATA_PATH;
    public static Long DATA_NUM_ROWS;
    public static Double DATA_LOWER_BOUND;
    public static Double DATA_UPPER_BOUND;
    public static Long DATA_ROWS_BETWEEN_OUTLIERS;

    public static Long GENERAL_STARTUP_WAIT;
    public static Boolean GENERAL_USE_SIMULATED_SCANNER;

    public static void init(String propertiesFilename) {
        try (InputStream input = EmitterConfiguration.class
                .getClassLoader().getResourceAsStream(propertiesFilename+".properties")) {

            properties = new Properties();

            if (input == null) {
                LOGGER.error("Unable to find "+propertiesFilename+".properties");
                return;
            }

            //load a properties file from class path, inside static method
            properties.load(input);

            KAFKA_ADDRESS = properties.getProperty("kafka.address");
            KAFKA_CLIENT_ID = properties.getProperty("kafka.client-id");
            KAFKA_TOPIC = properties.getProperty("kafka.topic");
            KAFKA_WAIT = Long.parseLong(properties.getProperty("kafka.wait"));
            KAFKA_LINGER_MS = Long.parseLong(properties.getProperty("kafka.linger.ms"));
            KAFKA_BATCH_SIZE = Integer.parseInt(properties.getProperty("kafka.batch.size"));

            DATA_PATH = properties.getProperty("data.data_path");
            DATA_NUM_ROWS = Long.parseLong(properties.getProperty("data.num-rows"));
            DATA_LOWER_BOUND = Double.parseDouble(properties.getProperty("data.lower-bound"));
            DATA_UPPER_BOUND = Double.parseDouble(properties.getProperty("data.upper-bound"));
            DATA_ROWS_BETWEEN_OUTLIERS = Long.parseLong(properties.getProperty("data.rows-between-outliers"));

            GENERAL_STARTUP_WAIT = Long.parseLong(properties.getProperty("general.startup-wait"));
            GENERAL_USE_SIMULATED_SCANNER = Boolean.parseBoolean(properties.getProperty("general.use-simulated-scanner"));

        } catch (IOException ex) {
            LOGGER.error("Error while loading properties.");
            ex.printStackTrace();
        }
    }

    public static String get(String propertyName){
        return properties.getProperty(propertyName);
    }
}
